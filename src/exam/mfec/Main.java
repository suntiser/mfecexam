/**
 * 
 */
package exam.mfec;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sunti
 *
 */
public class Main {

    private static int DATE      = 0;
    private static int START     = 1;
    private static int END       = 2;
    private static int NUMBER    = 3;
    private static int PROMOTION = 4;

    /**
     * 
     */
    public Main() {
    }

    public void generateJson(Map<String, Double> usage) {
        if (usage != null && !usage.isEmpty()) {
            int row = 0;
            System.out.println("No.\tNumber\tTime\tMin\tPay");
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, Double> entry : usage.entrySet()) {
                String number = entry.getKey();
                Double value = entry.getValue();
                int min = (int) (value / 60);
                min += value % 60 > 0 ? 1 : 0;
                double pay = 3 + (min - 1);
                
                System.out.println(++row + "\t" + number + "\t" + entry.getValue() + "\t" + min + "\t" + pay);
                builder.append(",{\"number\":\"").append(number).append("\",\"pay\":").append(pay).append("}");
            }
            String data = builder.toString();
            if(data.length() > 1) {
                FileOutputStream file = null;
                try {
                    file = new FileOutputStream("result.json");
                    file.write(("[" + data.substring(1) + "]").getBytes());
                }
                catch(IOException ioException) {
                    System.err.println("Cannot write json file");
                }
                finally {
                    if(file!= null) {
                        try {
                            file.close();
                            file = null;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public Map<String, Double> findUsage(String path) throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            throw new IOException("Promotion log not found '" + path + "'");
        }

        HashMap<String, Double> numbers = new HashMap<String, Double>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            if (reader.ready()) {
                String line = null;
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                while ((line = reader.readLine()) != null) {
                    String data[] = line.split("[|]");
                    try {
                        Date start = format.parse(data[DATE] + " " + data[START]);
                        Date end = format.parse(data[DATE] + " " + data[END]);
                        String number = data[NUMBER];
                        Double sum = numbers.get(number);
                        if (sum == null) {
                            sum = 0.0;
                        }
    
                        sum += (end.getTime() - start.getTime()) / 1000.0;
                        numbers.put(number, sum);
                    }
                    catch( ParseException parseException ) {
                        System.err.println( "Cannot convert start or end to date " + parseException.getMessage() );
                    }
                }
            }
        } catch (IOException ioException) {

        } finally {
            if (reader != null) {
                reader.close();
                reader = null;
            }
        }
        
        return numbers;
    }

    public static void main(String args[]) throws Exception {
        String path = "promotion1.log";
        Main main = new Main();
        Map<String, Double> usage = main.findUsage(path);
        main.generateJson(usage);
    }

}
